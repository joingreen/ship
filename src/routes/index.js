import { db } from '../gc'
const batch = db.batch()

export async function patch() {
    try {
        const data = db.collection('today').get()

        return {
            status: 200,
            body: data
        }

    } catch (e) {
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}

export async function post({ request }) {
    try {
        console.log('request')
        request.body; // shows as 'ReadableStream<Uint8Array>' type
        let chunks
        for await (const chunk of request.body) {
            chunks = chunk.toString()
        }
        const body = JSON.parse(chunks)

        const cityRef = db.collection('tody').doc(body.phone);

    // Set the 'capital' field of the city
        const res = await cityRef.update({status: body.status});

        return {
            status: 200
        }

    } catch (e) {
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}