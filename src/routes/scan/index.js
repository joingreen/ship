import { db } from '../../gc'
const batch = db.batch()

export async function post({ request }) {
    try {
        request.body; // shows as 'ReadableStream<Uint8Array>' type
        let chunks
        for await (const chunk of request.body) {
            chunks = chunk.toString()
        }
        const body = JSON.parse(chunks)

        console.log(body)
        body.data.forEach((d) => {
            const docRef = db.collection('today').doc(d.phone);
            batch.set(docRef, { des: d.des, loc: d.loc, status:0});
        });
        await batch.commit();

        return {
            status: 200
        }

    } catch (e) {
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}

