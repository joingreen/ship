import { db } from '../../gc'
const batch = db.batch()

function getTodayDate(day = 0){
    const today = new Date(Date.now()-day*24*60*60*1000)
    return today.toLocaleDateString('vi-VN',{day:'2-digit',month:'2-digit',year:'numeric'}).replace(/\//g,'-')
}

export async function patch({request}) {
    try {

        request.body; // shows as 'ReadableStream<Uint8Array>' type
        let chunks
        for await (const chunk of request.body) {
            chunks = chunk.toString()
        }
        const body = JSON.parse(chunks)

        const citiesRef = db.collection('shipOrder');
        const snapshot = await citiesRef.where('ngay', '==', getTodayDate(body.lastday)).get();
        if (snapshot.empty) {
            console.log('No matching documents.');
            return;
        }  
        let arr = []
        snapshot.forEach(doc => {
            arr.push({ ...doc.data()})
        });
        
        return {
            status: 200,
            body: arr
        }

    } catch (e) {
        console.log('patch /order')
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}
export async function post({ request }) {
    try {
        request.body; // shows as 'ReadableStream<Uint8Array>' type
        let chunks
        for await (const chunk of request.body) {
            chunks = chunk.toString()
        }
        const body = JSON.parse(chunks)

        const docRef = await db.collection('shipOrder').doc(body.phone).set({
            phone:body.phone,
            note: body.note,
            location:body.location,
            ngay:getTodayDate(),
            status:0,
            isnew:body.isnew
        })
      

        return {
            status: 200,
            body: {
                phone:body.phone,
                note: body.note,
                location:body.location,
                ngay:getTodayDate(),
                status:0,
                isnew:body.isnew
            }
        }

    } catch (e) {
        console.log('post /order')
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}


export async function put({ request }) {
    try {
        console.log('request')
        request.body; // shows as 'ReadableStream<Uint8Array>' type
        let chunks
        for await (const chunk of request.body) {
            chunks = chunk.toString()
        }
        const body = JSON.parse(chunks)

        const docRef = await db.collection('shipOrder').doc(body.phone).update({
            location:body.location,
            note: body.note,
            status: body.status
        })

        return {
            status: 200
        }

    } catch (e) {
        console.log('put')
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}


