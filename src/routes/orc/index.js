import vision from '@google-cloud/vision'
const CREDENTIAL = JSON.parse(JSON.stringify({
    "type": "service_account",
    "project_id": "sola-e9c81",
    "private_key_id": "df2039b1394855c35c8f99a6bdbbe5dc1b6ea798",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC7TrXaLWv/Ea3g\neA662zAxheIvKqbzOmD+8mVPoBTbA7ez/742gEyN07LTzGmVeXeXKPnEQ0pz3ERc\nVj9W6wm/YJqvzP/V0mOCnI0al7ZjpF0ll7mBD7LITGb2j0iaJ/6lM+6+P1mIXIBg\nPB3mGUeGMpdGaY3xc0eWbq/CNsvQ+gaWSXXm8zBYf3DnpkXAKj8OdJZJvLAMUpx0\nB5UU9yRLypCbDToeim6N7yEpjypybGKuZ6opj8rRhNaRmwacTD/c7JZVZzgYiBkZ\nAAZDIJprpMexshktBpdt8EwUtRIaS6LXDOXvlYrrBq81xdUCDcvytiIYqAB84/YM\nVqXTo2IzAgMBAAECggEADMcfqxzKehyZJC/chWJwOmNRR6QdE95e1pWaVAvDeyDD\noUu9jKyaTFntoFBDawHd0YSR7BHRHEVMWTuTAkQc0UmZqTY5b7tKCZN8zBx0bKi+\nUqhelyTcg+3eX6HtCIMuS9PWoh435pBfkPEY8w+0wZK69NU87ISk6hNupGJQ64ES\nOS1iywbYShgkMxgBpmd9rS1D5MnwLkZjPhe4A+O5OOpJhLIeCHZfZdm1qoueOyJz\nZGsBXYqKVAeY93mDO0lP9Sq2jD7kDQ8qEdM+oj+COkSe/WS02cUgm96p8hgtELJd\nIPsDbd40kN/3wykhrCrOt1qvIfm10D6O10XTS+wemQKBgQD3pR6g5v6ODMM5tOZ7\nz7yv8FtaF87qDRubPtkwXoYF4hn0TE1ClRkk2qId8mtptEvJxAwPRKpqp0Ex9Oje\nFuPC8jAy97VpjN31dm4cbwt9549GdzNaAna7ps/RqaI3cW8BLUbJZLgLpy5xt5Zb\nvP2VbeatIf9UrMxLJaLGql7OKQKBgQDBoHZtLtClo7Wr0FrpTb6Cs54Ls8aIcJOE\nTkD31gkltJ+lde1Spu/Uu+mNC3P8q3904LcjV5L59iSNEIZLevcZYZG8eXgcIsB9\n6mTrPcRKTuEHwpTVy3uiMYndxMiZDAL6IRh5xL9VnQGS9xiWu2bfhaMGG8gqrM79\nfUcENp1A+wKBgQDHJyLagulzkw/s+G3iY816L/FHIp5j2ua4knJXU6y0ToBQ1ovL\nX9w4pxp4uttmxnesatzH6pCh9J+ZbTtYX1ALFnlsg4iAuahGHleuxh6XwgCWyoWh\nfRm/DwdRtPmfXLnIWID0tOrNdEUFbn6ch//mi9tWoGCV1+DzDfE5kwlrAQKBgDx4\n1QO8f0V8H50NcZczxLAlUNLuzq6GGkxT9E5y9mBkTQfsELajlHuoRITcctkS5rem\nBwxuqWSPz1KPOqIQQWQX6OAoP67p25x3vBUMMWXTxMwbpUgwSOia+Cfe8Zd6/OMJ\nuw8st2URqBAyQ+evl61vmz6pJpbNacgZfDiAphRJAoGASN83WWQFrZa7HGu3YViG\nzDIqDG99RmWfezbBNZMKubqbh09FO1CbgEyi0Wa2vWeHN3raOOhz3MDSVrEzl/dg\npA17hDm0j3g8949STGkimLIEZUvjnLAFFz25x5NKRizkcBa/pNh5z0LqPAp/W8zS\nh1nS4NgOGCnyClbR79RO6J0=\n-----END PRIVATE KEY-----\n",
    "client_email": "vison-57@sola-e9c81.iam.gserviceaccount.com",
    "client_id": "118122515139711317065",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/vison-57%40sola-e9c81.iam.gserviceaccount.com"
}))
const CONFIG = {
    credentials: {
        private_key: CREDENTIAL.private_key,
        client_email: CREDENTIAL.client_email
    }
}
const client = new vision.ImageAnnotatorClient(CONFIG);

export async function post({request}) {
    try {

        request.body; // shows as 'ReadableStream<Uint8Array>' type
        let chunks
        for await (const chunk of request.body) {
            chunks = chunk.toString()
        }
        const body = JSON.parse(chunks)
        console.log(body)
        
        let r = []

        const file = Buffer.from(body.img, 'base64');
        const [result] = await client.textDetection(file);
        const detections = result.textAnnotations
        const text = detections[0].description;
        const txt = text.match(/.+\n(0|84)\d{9}\n.+\n.+\n.+/g)
        txt.map((v,i)=>{
            const phone = v.match(/(0|84)\d{9}/)[0]
            const note = v.split(phone).toString()

            r.push({phone:phone, note:note})
        })
        
        if(body.img1){
            const file = Buffer.from(body.img1, 'base64');
            const [result] = await client.textDetection(file);
            const detections = result.textAnnotations
            const text = detections[0].description;

            const txt = text.match(/.+\n(0|84)\d{9}\n.+\n.+\n.+/g)
            txt.map((v,i)=>{
                const phone = v.match(/(0|84)\d{9}/)[0]
                const note = v.split(phone).toString()

                r.push({phone:phone, note:note})
            })
        }

        console.log(r)
        return {
            status: 200,
            body: r
        }
 

    } catch (e) {
        console.log('Error: ' + e.message)
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}


const extract = (text) =>{
    const infos = text.match(/.+\n0\d{9}(.+\n)+/g)
    console.log(infos)
}
