import type { RequestHandler } from '@sveltejs/kit';
import {db} from '../../gc'

export const post: RequestHandler = async ({ request }) => {
   
    request.body; // shows as 'ReadableStream<Uint8Array>' type
    let chunks
    for await (const chunk of request.body) {
       chunks = chunk.toString()
    }
    const body = JSON.parse(chunks)
    console.log(body)

    db.collection('phoneNumber').doc(body.id).set({
        lat:body.lat,
        lng:body.lng
    })
    
    return { status: 200 };
};