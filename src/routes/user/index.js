import { db } from '../../gc'
const batch = db.batch()

export async function del({ request }) {
    try {
        
        request.body; // shows as 'ReadableStream<Uint8Array>' type
        let chunks
        for await (const chunk of request.body) {
            chunks = chunk.toString()
        }
        const body = JSON.parse(chunks)
        console.log(body)
        const doc = await db.collection("shipUser").doc(body.phone).get()
        
        return {
            status:200,
            body:doc.data()
        }
       

    } catch (e) {
        console.log('patch /user')
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}


export async function patch({ request }) {
    try {
        
        request.body; // shows as 'ReadableStream<Uint8Array>' type
        let chunks
        for await (const chunk of request.body) {
            chunks = chunk.toString()
        }
        const body = JSON.parse(chunks)
        console.log(body)
        const resultArrPhone = []

        const snap = await db.collection("shipUser").where("phone", "in", body.arrPhone).get()
        
        snap.forEach((doc) => {
            if(doc.exists){
                resultArrPhone.push(doc.data())
            }
        });
        return {
            status:200,
            body:resultArrPhone
        }
       

    } catch (e) {
        console.log('patch /user')
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}

export async function post({ request }) {
    try {
        console.log('request')
        request.body; // shows as 'ReadableStream<Uint8Array>' type
        let chunks
        for await (const chunk of request.body) {
            chunks = chunk.toString()
        }
        const body = JSON.parse(chunks)

        let docRef = await db.collection('shipUser').doc(body.phone).set({ 
            phone: body.phone,
            note:body.note,
            location: body.location
        })

        return {
            status: 200,
            body:{ 
                phone: body.phone,
                note:body.note,
                location: body.location
            }
        }

    } catch (e) {
        console.log('patch /post')
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}

export async function put({ request }) {
    try {
        console.log('request')
        request.body; // shows as 'ReadableStream<Uint8Array>' type
        let chunks
        for await (const chunk of request.body) {
            chunks = chunk.toString()
        }
        const body = JSON.parse(chunks)
        console.log(body)
        let docRef = await db.collection('shipUser').doc(body.phone).update({
            note:body.note,
            location: body.location
        })

        return {
            status: 200,
            body:{ 
                phone: body.phone,
                note:body.note,
                location: body.location
            }
        }

    } catch (e) {
        console.log('patch /put')
        console.log(e.message)
        return {
            status: 400,
            body: { error: e.message }
        }
    }
}