import { initializeApp,  cert} from 'firebase-admin/app';
import { getFirestore} from 'firebase-admin/firestore';

initializeApp({
  credential: cert(JSON.parse(JSON.stringify({
    "type": "service_account",
    "project_id": "sola-e9c81",
    "private_key_id": "0122cd4bd9de77ff7fa4216d967759d9b43243b5",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC5bM7se+n9TODy\njf3oz116qjQKNUhwzNDkJwtcSSntSuhpnJ++iBWYbSZE1uRgBfvDXXSPdJ06dMv4\nRxFRyUz38ScJnJrQej51bC/mx00crznC+3/zrJ/1QZLTFGEyVk1ByeBZuIXtD/YC\nFJTX/vKzXIsRdmyHKL/0Lbwq9+70MeVGi2JQ3J7+BLzVmuhFKp3XDNG2zhKdVSVS\nUnGPIkf2GeADvh7ZnyzUpRBsbuPi/6NDAsAZ7H0+47NcX5i3PVGeFYUrvqeRR680\n/R7N/HnSJwFi2Inh9xMugKH8LkpbifKnraCV4Wh5fU7f9kqhpdek0Pb/IWjTfhHR\nBhnrdSB9AgMBAAECggEAIL6/3hBV9pb8hKkhpVr1aaOkFAdr1wu8egYRIgeDoXWg\njIINzdJvad8gqnh+lRyJZOtcmbbgy0F0AdDfFJ/leUW7fJ2M03M7E3Z7ERiyG/Yv\nz6ilZG1gRCoD2UXYklCeTRdvCLkm4YH3kk97J5fM2XLm1jshGE9Qcrke6UGmj7Xx\nETEXrL6Zp1pFrfXYJE7OXWDTb/90SJ/hUegrfHDBfSHJ4lJTLO3dCk90wRa2r8kr\n/Ouu8A9sQcBoM25ixm3muKrTfc8O1Z6trvWLDzYIV7qExbpaALaG3S+U7BW8fkMJ\n6TTh7t52GoPj++X4cEMNsTlSOpt/TGrrJF3I/rHMcQKBgQDaIaPnzPIorxzO0mXt\nOl9lcFKF/YtaYT8iDNa2ph3krgNpOl0FjI7BpKsh4I0iOp8vnQyx1uUxIFD6pJ3M\nrC+uiPwrjF5cuZEoUkoHSrF+O0UH6kpGj68O4EIyPyeHSxG+kYOBp4SJQsDOpFAP\numKOQOnNegR1N5E0M1At/cg3CQKBgQDZnZtVzO/zNP6fpiknzwRcun1y+obO7XVn\ngrir9HsSUM7JEZhGpsTWSUJLWQekHf1ANRTh4n21AZfMjS+mImsqfxg37nxFNZOo\n7KX5fbd9IF6YgTDNkRxwkJevrqJZSfftvbxADKs4RHYsB1wfULISvOaFkmQOipyo\nuNeAhxQm1QKBgQCGR5822xtXoDUDvNGyL/T55SNoCKT4hlTvCShEWA6e7+B5rswE\n1norCsuulyVKpPvJu8xMzhVZIKO5KA1cKcNQllNmG4sbWcDaKY4Y3wpUwvfD357J\nwTnq1nLpq6cRykyqG7nw3sbgGI/Ipa4DgKGbIKrNKwgDgKmKkOwS49+UwQKBgQDM\nRGnDvyvZAfykRSdKi8LPe+o3nDwgTrsun4DTQBiqF5oTGOX0I6SOdJO7AGXwwomu\n1GS2p5BR8J3ZzwlUC+jGKijEj6KsoF7raL9fEofC9zh0NIhn3XdxnfgUW3tfDtSf\nn8Lt6DZsCa9XIs1xQ6Dzf8dux/wNvkTRBziaF7FtxQKBgFmulso+98di1ekNMhM/\nG9M4qUe++Jmql2FnMFHA2oKwwkHgfuxvw3TEe29IaNZczZXssAJOie72SzSOsy12\nsK3WWveu68T9GHVnoEWRSec9ghpQ/N2fjyYBMZn9tgpQ8B+5JCnC8h8q138cgY4B\nizAGuF2/vPBn9mpfW85OApGX\n-----END PRIVATE KEY-----\n",
    "client_email": "firebase-adminsdk-wjoqq@sola-e9c81.iam.gserviceaccount.com",
    "client_id": "105145476412724702903",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-wjoqq%40sola-e9c81.iam.gserviceaccount.com"
  })))
  ,
  storageBucket: 'sola-e9c81.appspot.com'
});

export const db = getFirestore()