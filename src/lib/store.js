import { writable} from 'svelte/store'

const dataStoreS = () => {
	const { subscribe, set, update } = writable({})
	return {
		subscribe,
		set:(data) => set(data),
		update:(data) => update(current =>{
			return {...current, ...data}
            
		})
	}
}
export const dataStore = dataStoreS()


const locationStoreS = () => {
	const { subscribe, set } = writable("")
	return {
		subscribe,
		set:(data) => set(data)
	}
}
export const locationStore = locationStoreS()

